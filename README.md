THORChain Template
=============

UI for to start a new THORChain site.

## Setup
```bash
yarn
```

## Start
```bash
yarn start
```