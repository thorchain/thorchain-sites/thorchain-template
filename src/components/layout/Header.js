import React, { useContext } from 'react'
import { Layout, Row, Col} from 'antd';

import { Center, Icon, Button } from '../Components'

const Header = (props) => {

  const styles = {
    position: 'fixed',
    zIndex: 1,
    width: '100%',
    backgroundColor:"#2B3947",
  }

  return (

        <Layout.Header className="header-container" style={styles} >
          <Row>
            <Col sm={8} md={4}>
                <Icon icon="runelogo" style={{height:40}} />
            </Col>
            <Col sm={8} md={14}>
              <Center><span>STAKE AND EARN RUNE</span></Center>
            </Col>
            <Col sm={8} md={6}>
            </Col>
          </Row>
      </Layout.Header>
)
}

export default Header
