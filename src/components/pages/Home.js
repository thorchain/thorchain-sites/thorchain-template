import React from 'react'
import { Row, Col, Table } from 'antd';

import { Icon, H1, Button, Text } from '../Components'

const homeStyles = {
    marginLeft: 0,
    marginTop: 40,
    backgroundColor: "#101921"
}

const iconStyles = {
    marginTop: 140,
    backgroundColor: "#101921"
}

const Home = () => {

    return (
        <div style={{ backgroundColor: "#101921" }}>
            <Row style={{}}>
                <Col xs={24} sm={1} md={2} lg={3}>
                </Col>

                <Col xs={24} sm={12} md={12} lg={9} style={homeStyles}>

                    <H1>STAKE RUNE AND EARN</H1>
                    <br></br>
                    <h4 style={{ color: "#848E9C" }}><span>STAKE RUNE TO EARN WEEKLY 1% INTEREST UNTIL THE LAUNCH OF </span>
                        <span><strong><a href="/" style={{ color: "#fff" }}>BEPSWAP</a></strong></span>
                    </h4>
                    <br></br>
                    <p>1) Stake your RUNE using this interface.</p>
                    <p>2) Earn weekly 1% (52% APR) of your balance compounded until the launch of BEPSwap.</p>
                    <p>3) When BEPSwap launches, withdraw and stake your total earnings in a liquidity pool and
                                continue earning. </p>
                    <br></br>
                        <Button style={{ height: 40, width: 250 }}>STAKE NOW</Button>
                    <br></br>
                    <br></br>

                </Col>

                <Col xs={24} sm={2} md={2} lg={2}>
                </Col>

                    <Col xs={24} sm={8} md={8} lg={9} style={iconStyles}>
                        <Icon icon="rune" style={{ width: 450 }} />
                    </Col>

                <Col xs={24} sm={1} md={2} lg={3}>
                </Col>

            </Row>
        </div>
    )

}

export default Home
